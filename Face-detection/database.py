import sqlite3

conn = sqlite3.connect('database.db')

c = conn.cursor()

sql = """
DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
           id integer unique primary key autoincrement,
           name text
);
"""
c.executescript(sql)

conn.commit()

conn.close()
